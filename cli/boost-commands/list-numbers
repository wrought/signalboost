#!/usr/bin/env bash

if [[ $1 == "-h" ]] || [[ $1 == "--help" ]];then
  echo "This command retrieves active phone numbers. You may find it useful for:"
  echo ""
  echo "(1) discovering inactive phone numbers to use for new channels"
  echo "(2) retrieving twilio sids for releasing phone numbers (with boost release_numbers)"
  echo ""
  echo "valid options are:"
  echo ""
  echo "--active : filter for active phone numbers"
  echo "--inactive : filter for inactive phone numbers"
  echo "--len : only show phone number list lenght (omit list itself)"
  echo "-u : url to target (in dev, use signalboost.ngrok.io)";
  echo ""
  exit 1
fi

echo "--- checking environment..."

if [ -z $SIGNALBOOST_API_TOKEN ];then
  echo "--- ERROR: no SIGNALBOOST_API_TOKEN found. try \`set -a && source .env && set +a\`"
  exit 1
fi


while getopts ":u:" opt; do
  case "$opt" in
    u)
      url="$OPTARG"
      ;;
  esac
done

if [[ $1 == "--active" ]] || [[ $2 == "--active" ]];then
  filter="ACTIVE"
fi

if [[ $1 == "--inactive" ]] || [[ $2 == "--inactive" ]];then
  filter="INACTIVE"
fi

if [[ $1 == "--len" ]] || [[ $2 == "--len" ]];then
  selector='.count'
fi

if [ -z $filter ]
then
  query_string=""
else
  query_string="?filter=$filter"
fi

if [ -z $url ];then url=${SIGNALBOOST_HOST_URL}; fi

echo "--- fetching numbers..."

curl -s \
     -H "Content-Type: application/json" \
     -H "Token: $SIGNALBOOST_API_TOKEN" \
     https://${url}/phoneNumbers${query_string} | jq ${selector}
