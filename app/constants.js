// TODO: move statuses to util and kill this file!
const statuses = {
  SUCCESS: 'SUCCESS',
  NOOP: 'NOOP',
  ERROR: 'ERROR',
}

// TODO: move languages to somewhere more language-y!
const languages = {
  EN: 'EN',
}

module.exports = { statuses, languages }
